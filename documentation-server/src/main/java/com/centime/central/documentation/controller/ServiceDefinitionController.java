package com.centime.central.documentation.controller;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.centime.central.documentation.config.swagger.ServiceDefinitionsContext;

@RestController
@Validated
public class ServiceDefinitionController {

	@Autowired
	private ServiceDefinitionsContext definitionContext;

	@GetMapping("/service/{servicename}")
	public String getServiceDefinition(@PathVariable("servicename") @NotBlank String serviceName) {

		return definitionContext.getSwaggerDefinition(serviceName);

	}
}
