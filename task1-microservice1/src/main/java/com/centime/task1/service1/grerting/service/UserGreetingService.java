package com.centime.task1.service1.grerting.service;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.centime.task1.service1.service.delegator.HelloServiceDelegator;
import com.centime.task1.service1.service.delegator.UserNameServiceDelegator;
import com.centime.task1.service1.user.model.User;

@Service
public class UserGreetingService {

	@Autowired
	private UserNameServiceDelegator userNameServiceDelegator;

	@Autowired
	private HelloServiceDelegator helloServiceDelegator;

	public String greetUser(@Valid @RequestBody User user) {
		String helloString = helloServiceDelegator.getHelloFromHelloService();
		String nameString = userNameServiceDelegator.getUserNameFromUserNameBuilderService(user);
		return StringUtils.join(new String[] { helloString, " ", nameString });
	}

}
