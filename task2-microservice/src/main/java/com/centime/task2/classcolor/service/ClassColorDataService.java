package com.centime.task2.classcolor.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.centime.core.exception.ServiceException;
import com.centime.task2.classcolor.model.ClassColorData;

public interface ClassColorDataService {

	List<ClassColorData> listAll();

	ClassColorData getById(@NotNull @Min(1) Long id) throws ServiceException;

	ClassColorData create(@Valid ClassColorData classColorData) throws Exception;

}
