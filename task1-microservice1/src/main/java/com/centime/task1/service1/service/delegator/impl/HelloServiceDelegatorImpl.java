package com.centime.task1.service1.service.delegator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.centime.task1.service1.service.delegator.HelloServiceDelegator;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class HelloServiceDelegatorImpl implements HelloServiceDelegator {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${service.name.task1.microservice2}")
	private String helloGreetingServiceName;

	@HystrixCommand(fallbackMethod = "helloFallBack", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000")

	})
	@Override
	public String getHelloFromHelloService() {
		String helloString = restTemplate.getForObject(String.format("http://%s/hello", helloGreetingServiceName),
				String.class);
		return helloString;
	}

	@SuppressWarnings("unused")
	private String helloFallBack() {
		return "Hello (built by fallback)";
	}

}
