package com.centime.central.documentation.config.swagger;

import java.time.LocalDate;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ServiceDescriptionUpdater {

	private static final Logger logger = LoggerFactory.getLogger(ServiceDescriptionUpdater.class);

	private static final String DEFAULT_SWAGGER_URL = "/v2/api-docs";

	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private RestTemplate template;

	@Autowired
	private ServiceDefinitionsContext definitionContext;

	@Scheduled(fixedDelayString = "${swagger.config.refreshrate}")
	public void refreshSwaggerConfigurations() {
		logger.info("Starting Service Definition Context refresh");
		discoveryClient.getServices().stream().forEach(serviceId -> {

			List<ServiceInstance> serviceInstances = discoveryClient.getInstances(serviceId);
			if (CollectionUtils.isNotEmpty(serviceInstances)) {
				String swaggerURL = getSwaggerURL(serviceId);
				Object jsonData = getSwaggerDefinitionForAPI(serviceId, swaggerURL);
				if (jsonData != null) {
					String content = getJSON(serviceId, jsonData);
					definitionContext.addServiceDefinition(serviceId, content);
				} else {
					logger.error("Skipping service id : {} Error : Could not get Swagegr definition from API ",
							serviceId);
				}
				logger.info("Service Definition Context Refreshed at :  {}", LocalDate.now());
			}
		});
	}

	private String getSwaggerURL(String serviceId) {
		return String.format("http://%s%s", serviceId, DEFAULT_SWAGGER_URL);
	}

	private Object getSwaggerDefinitionForAPI(String serviceName, String url) {
		logger.info("Accessing the SwaggerDefinition JSON for Service : {} : URL : {} ", serviceName, url);
		try {
			return template.getForObject(url, Object.class);
		} catch (RestClientException ex) {
			logger.error("Error while getting service definition for service : {} Error : {} ", serviceName,
					ex.getMessage());
		}
		return null;

	}

	public String getJSON(String serviceId, Object jsonData) {
		try {
			return new ObjectMapper().writeValueAsString(jsonData);
		} catch (JsonProcessingException e) {
			logger.error("Error : {} ", e.getMessage());
		}
		return "";
	}
}