package com.centime.task1.service1.web.greeting.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.centime.core.loggable.LogMethodParam;
import com.centime.task1.service1.grerting.service.UserGreetingService;
import com.centime.task1.service1.user.model.User;

@RestController
public class UserGreetingController {

	@Autowired
	private UserGreetingService userGreetingService;

	@PostMapping(path = "/greeting")
	@LogMethodParam
	public String greetUser(@Valid @RequestBody User user) {
		return userGreetingService.greetUser(user);
	}

}
