package com.centime.task2.classcolor.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import com.centime.core.exception.ServiceException;
import com.centime.core.exception.ValidationException;
import com.centime.task2.classcolor.dao.ClassColorDataDAO;
import com.centime.task2.classcolor.model.ClassColorData;
import com.centime.task2.classcolor.service.ClassColorDataService;

@Service
@Validated
public class ClassColorDataServiceImpl implements ClassColorDataService {

	private Logger logger = LoggerFactory.getLogger(ClassColorDataServiceImpl.class);

	@Autowired
	private ClassColorDataDAO dao;

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public List<ClassColorData> listAll() {
		List<ClassColorData> classColorDataList = dao.findAll();
		Map<Long, List<ClassColorData>> parentSubClassMap = new HashMap<>();
		for (ClassColorData classColorData : classColorDataList) {
			ClassColorData parentClassColor = classColorData.getParentClassColor();
			if (parentClassColor != null) {
				List<ClassColorData> childSubclasses = parentSubClassMap.get(parentClassColor.getId());
				if (childSubclasses == null) {
					childSubclasses = new ArrayList<>();
					parentSubClassMap.put(parentClassColor.getId(), childSubclasses);
				}
				childSubclasses.add(classColorData);

			}
		}
		populateSubcClassColorDataForList(classColorDataList, parentSubClassMap);
		return classColorDataList;
	}

	private void populateSubcClassColorDataForList(List<ClassColorData> classColorDataList,
			Map<Long, List<ClassColorData>> parentSubClassMap) {
		if (!CollectionUtils.isEmpty(classColorDataList)) {
			for (ClassColorData classColorData : classColorDataList) {
				populateSubClassColorData(classColorData, parentSubClassMap);
			}
		}
	}

	private void populateSubClassColorData(ClassColorData classColorData,
			Map<Long, List<ClassColorData>> parentSubClassMap) {
		List<ClassColorData> subClassColors = parentSubClassMap.get(classColorData.getId());
		if (!CollectionUtils.isEmpty(subClassColors)) {
			classColorData.setSubClassColors(subClassColors);
			populateSubcClassColorDataForList(subClassColors, parentSubClassMap);
		}

	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public ClassColorData getById(@NotNull @Min(1) Long id) throws ServiceException {
		if (dao.existsById(id)) {
			return dao.getOne(id);
		}
		throw new ServiceException("Invalid id, no class color data exist with id: " + id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public ClassColorData create(@Valid ClassColorData classColorData) throws Exception {
		classColorData.setId(null);
		validateParentClassColorData(classColorData);
		return dao.save(classColorData);
	}

	private void validateParentClassColorData(ClassColorData classColorData) throws ValidationException {
		ClassColorData parentClassColor = classColorData.getParentClassColor();
		if (parentClassColor != null) {
			if (parentClassColor.getId() == null || parentClassColor.getId() < 1) {
				logger.error("Invalid parent class Id: {}", parentClassColor.getId());
				throw new ValidationException("Invalid parent class Id: " + parentClassColor.getId());
			}
			Optional<ClassColorData> parentClass = dao.findById(parentClassColor.getId());
			if (!parentClass.isPresent()) {
				logger.error("Invalid parent class data, Parent class data does not exist with id: {}",
						classColorData.getId());
				throw new ValidationException("Invalid parent class data, Parent class data does not exist with id: "
						+ classColorData.getId());
			}
			classColorData.setParentClassColor(parentClass.get());
		}
	}

}
