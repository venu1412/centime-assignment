package com.centime.central.documentation.config.swagger;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.swagger.web.InMemorySwaggerResourcesProvider;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

@Configuration
public class SwaggerUIConfiguration {

	@Autowired
	private ServiceDefinitionsContext definitionContext;

	@Primary
	@Bean
	@Lazy
	public SwaggerResourcesProvider swaggerResourcesProvider(InMemorySwaggerResourcesProvider defaultResourcesProvider,
			RestTemplate temp) {
		return new SwaggerResourcesProvider() {
			@Override
			public List<SwaggerResource> get() {
				List<SwaggerResource> resources = new ArrayList<>(defaultResourcesProvider.get());
				resources.clear();
				resources.addAll(definitionContext.getSwaggerDefinitions());
				return resources;
			}
		};
	}
}