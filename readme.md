### Technologies & Approach implemented

1. Used Eureka discovery management for service discovery
2. Used Spring cloud config server for centralized configuration management (config-server)
3. Used Histrix for fault tolerance & resilliance. (Implemented in task1-service1 as it communicates with other services like task1-service2 & task1-service3)
4. Implemented a centralized documentation server. All micro-services swagger documentation can be found from the swagger UI of documentation-server.
5. Implemented cors as there is distributed documentation-server (Centralized Swagger UI), with out cors we can not call other services from centralized swagger documentation.
6. used in-memory H2 database for task2.

## Deployment details
Deployed all the microservices on AWS ec2. details of the server as follows

1. **Eureka Server:** http://3.7.154.179:8761/
2. **Centralized documentation Swagger:** http://3.7.154.179:8084/swagger-ui.html
3. **Task1 Microservice1 Swagger:** http://3.7.154.179:8080/swagger-ui.html
4. **Task1 Microservice2 Swagger:** http://3.7.154.179:8081/swagger-ui.html
5. **Task1 Microservice3 Swagger:** http://3.7.154.179:8082/swagger-ui.html
6. **Task2 Microservice Swagger:** http://3.7.154.179:8083/swagger-ui.html
7. **Centalized Config Server:** http://3.7.154.179:8888/swagger-ui.html
