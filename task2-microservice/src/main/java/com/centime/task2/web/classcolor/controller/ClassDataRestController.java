package com.centime.task2.web.classcolor.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.centime.core.exception.ServiceException;
import com.centime.core.loggable.LogMethodParam;
import com.centime.task2.classcolor.model.ClassColorData;
import com.centime.task2.classcolor.service.ClassColorDataService;

@RestController
@RequestMapping("/class-color")
@Validated
public class ClassDataRestController {

	@Autowired
	private ClassColorDataService classColorDataService;

	@GetMapping(path = "/")
	public List<ClassColorData> list() {
		return classColorDataService.listAll();
	}

	@GetMapping(path = "/{id}")
	@LogMethodParam
	public ClassColorData get(@NotNull @Min(1) @PathVariable("id") Long id) throws ServiceException {
		return classColorDataService.getById(id);
	}

	@PostMapping(path = "/")
	@LogMethodParam
	public ClassColorData create(@RequestBody @Valid ClassColorData classColorData) throws Exception {
		return classColorDataService.create(classColorData);
	}

	@PostConstruct
	private void initializeData() throws Exception {
		String[][] data = new String[][] { new String[] { "Warrior", "red", null },
				new String[] { "Wizard", "green", null }, new String[] { "Priest", "white", null },
				new String[] { "Rogue", "yellow", null }, new String[] { "Fighter", "blue", "1" },
				new String[] { "Paladin", "lighblue", "1" }, new String[] { "Ranger", "lighgreen", "1" },
				new String[] { "Mage", "grey", "2" }, new String[] { "Specialist wizard", "lightgrey", "2" },
				new String[] { "Cleric", "red", "3" }, new String[] { "Druid", "green", "3" },
				new String[] { "Priest of specific mytho", "white", "3" }, new String[] { "Thief", "yellow", "4" },
				new String[] { "Bard", "blue", "4" }, new String[] { "Bard", "lighblue", "13" } };

		for (String[] classColorData : data) {
			create(new ClassColorData(classColorData[0], classColorData[1],
					classColorData[2] != null ? new ClassColorData(new Long(classColorData[2])) : null));

		}
	}

}
