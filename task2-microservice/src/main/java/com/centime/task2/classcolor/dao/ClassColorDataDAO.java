package com.centime.task2.classcolor.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.centime.task2.classcolor.model.ClassColorData;

public interface ClassColorDataDAO extends JpaRepository<ClassColorData, Long> {

}
