package com.centime.task1.service1.service.delegator.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.centime.task1.service1.service.delegator.UserNameServiceDelegator;
import com.centime.task1.service1.user.model.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class UserNameServiceDelegatorImpl implements UserNameServiceDelegator {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${service.name.task1.microservice3}")
	private String userNameBuilderServiceName;

	@HystrixCommand(fallbackMethod = "userNameFallBack", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000") })
	@Override
	public String getUserNameFromUserNameBuilderService(User user) {
		String nameString = restTemplate.postForObject(
				String.format("http://%s/user-name-builder", userNameBuilderServiceName), user, String.class);
		return nameString;
	}

	@SuppressWarnings("unused")
	private String userNameFallBack(User user) {
		return StringUtils.join(new String[] { user.getName(), " ", user.getSurName(), " (built by fallback)" });
	}
}
