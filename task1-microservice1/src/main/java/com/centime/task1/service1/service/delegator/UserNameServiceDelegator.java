package com.centime.task1.service1.service.delegator;

import com.centime.task1.service1.user.model.User;

public interface UserNameServiceDelegator {

	String getUserNameFromUserNameBuilderService(User user);

}
