package com.centime.task1.service1.status.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

	@GetMapping(path = "/status")
	public String getServerStatus() {
		return "Up";
	}

}
