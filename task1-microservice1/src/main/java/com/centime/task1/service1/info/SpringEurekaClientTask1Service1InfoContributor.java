package com.centime.task1.service1.info;

import java.util.Collections;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

@Component
public class SpringEurekaClientTask1Service1InfoContributor implements InfoContributor {

	@Override
	public void contribute(Info.Builder builder) {
		builder.withDetail("details", Collections.singletonMap("description",
				"This is the User Greeting Service (Task1 - Service1), which is discovery server aware, and this service will Call Hello Microservice and User-Name-builder-service"));
	}

}
