package com.centime.core.loggable;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogMethodParamAspect {

	@Before("@annotation(LogMethodParam)")
	public void logExecutionTime(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		LogMethodParam logMethodParam = method.getAnnotation(LogMethodParam.class);
		boolean logParams = logMethodParam.params();
		if (logParams) {
			if (joinPoint.getArgs() != null && joinPoint.getArgs().length > 0) {
				StringBuilder sb = new StringBuilder();
				for (int index = 0; index < joinPoint.getArgs().length; index++) {
					sb.append(method.getParameterTypes()[index].getName() + ":" + joinPoint.getArgs()[index]);
					if (index < joinPoint.getArgs().length - 1)
						sb.append(", ");
				}
				Logger logger = LoggerFactory.getLogger(method.getDeclaringClass());
				logger.info("Class: {} Method: {}, Args: {}", method.getDeclaringClass().getName(), method.getName(),
						sb);
			}
		}
	}
}
