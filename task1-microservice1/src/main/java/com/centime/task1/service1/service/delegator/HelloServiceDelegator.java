package com.centime.task1.service1.service.delegator;

public interface HelloServiceDelegator {

	String getHelloFromHelloService();

}
