package com.centime.core.exception.handler.advice;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.centime.core.exception.ServiceException;
import com.centime.core.exception.ValidationException;
import com.centime.core.exception.handler.ErrorResponse;

@ControllerAdvice
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

	private Logger logger = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error("Invalid Method Argument: {}", ex.getMessage());
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream()
				.map(error -> error.getField() + " " + error.getDefaultMessage()).collect(Collectors.toList());
		ErrorResponse errorResponse = buildErrorResponse(status, errorMessages);
		return new ResponseEntity<>(errorResponse, headers, status);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ErrorResponse handleInternalServerError(Exception e) {
		logger.error("Internal server error", e);
		String errorMessage = String.format("Internal Server Error (traceId: %s)", MDC.get("X-B3-TraceId"));
		return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, Arrays.asList(errorMessage));
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ValidationException.class)
	@ResponseBody
	public ErrorResponse handleValidationException(ValidationException validationException) {
		logger.error("Invalid input for request", validationException);
		return buildErrorResponse(HttpStatus.BAD_REQUEST, Arrays.asList(validationException.getMessage()));
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ServiceException.class)
	@ResponseBody
	public ErrorResponse handleServiceException(ServiceException serviceException) {
		logger.error("Error while serving the request, %s", serviceException.getMessage());
		String errorMessage = String.format("Error while serving the request, %s (traceId: %s)",
				serviceException.getMessage(), MDC.get("X-B3-TraceId"));
		return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, Arrays.asList(errorMessage));
	}

	private ErrorResponse buildErrorResponse(HttpStatus status, List<String> errorMessages) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setStatus(status.toString());
		errorResponse.setMessage(errorMessages);
		return errorResponse;
	}

}
