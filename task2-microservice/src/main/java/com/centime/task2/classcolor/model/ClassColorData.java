package com.centime.task2.classcolor.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a ClassColorData in the application.")
@Entity
@JsonInclude(value = Include.NON_EMPTY)
public class ClassColorData {

	@Id
	@ApiModelProperty(notes = "Id of the class.", example = "1", position = 1)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ApiModelProperty(notes = "Name of the class.", example = "Warrior", required = true, position = 2)
	@NotBlank
	@Size(max = 50)
	private String name;

	@ApiModelProperty(notes = "Color of the class.", example = "Red", required = true, position = 3)
	@NotBlank
	@Size(max = 50)
	private String color;

	@ApiModelProperty(notes = "Parent class data.", position = 4)
	@JsonBackReference
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "parentId")
	private ClassColorData parentClassColor;

	@ApiModelProperty(notes = "subclass colors data.", position = 5)
	@Transient
	private Collection<ClassColorData> subClassColors;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public ClassColorData getParentClassColor() {
		return parentClassColor;
	}

	public void setParentClassColor(ClassColorData parentClassColorData) {
		this.parentClassColor = parentClassColorData;
	}

	public Collection<ClassColorData> getSubClassColors() {
		return subClassColors;
	}

	public void setSubClassColors(Collection<ClassColorData> subClassColors) {
		this.subClassColors = subClassColors;
	}

	public ClassColorData() {
		super();
	}

	public ClassColorData(Long id) {
		super();
		this.id = id;
	}

	public ClassColorData(@NotBlank @Size(max = 50) String name, @NotBlank @Size(max = 50) String color,
			ClassColorData parentClassColor) {
		super();
		this.name = name;
		this.color = color;
		this.parentClassColor = parentClassColor;
	}

	@Override
	public String toString() {
		return "ClassColorData [" + (id != null ? "id=" + id + ", " : "") + (name != null ? "name=" + name + ", " : "")
				+ (color != null ? "color=" + color + ", " : "")
				+ (parentClassColor != null ? "parentClassColor=" + parentClassColor + ", " : "")
				+ (subClassColors != null ? "subClassColors=" + subClassColors : "") + "]";
	}

}
