package com.centime.task1.service3.user.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a user in the application.")
public class User {

	@ApiModelProperty(notes = "First Name of the User.", example = "Narendra Modi", required = true, position = 1)
	@NotBlank
	@Size(max = 50)
	private String name;

	@ApiModelProperty(notes = "SurName Name of the User.", example = "Damodardas", required = true, position = 2)
	@NotBlank
	@Size(max = 50)
	private String surName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	@Override
	public String toString() {
		return "User [" + (name != null ? "name=" + name + ", " : "") + (surName != null ? "surName=" + surName : "")
				+ "]";
	}

}
