package com.centime.task1.service3.info;

import java.util.Collections;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

@Component
public class SpringEurekaClientTask1Service3InfoContributor implements InfoContributor {

	@Override
	public void contribute(Info.Builder builder) {
		builder.withDetail("details", Collections.singletonMap("description",
				"This is the User Name Builder Service (Task1 - Service3), which is discovery server aware, This service will return user Name by concatinating name & surname"));
	}
}
