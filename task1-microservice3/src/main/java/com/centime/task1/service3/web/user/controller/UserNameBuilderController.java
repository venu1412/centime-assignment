package com.centime.task1.service3.web.user.controller;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.centime.core.loggable.LogMethodParam;
import com.centime.task1.service3.user.model.User;

@RestController
@Validated
public class UserNameBuilderController {

	private Logger logger = LoggerFactory.getLogger(UserNameBuilderController.class);

	@PostMapping(path = "/user-name-builder")
	@LogMethodParam
	public String buildUserNameForGreeting(@Valid @RequestBody User user) {
		logger.info("create user name builder request for {}", user);
		return StringUtils.join(new String[] { user.getName(), " ", user.getSurName() });
	}

}
