package com.centime.task1.service2.web.hello.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResourceController {

	private Logger logger = LoggerFactory.getLogger(HelloResourceController.class);

	@GetMapping(path = "/hello")
	public String getUserGreeting() {
		logger.info("Hello string Greeting request");
		return "Hello";
	}

}
